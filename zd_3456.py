# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 10:51:41 2015

@author: laptop
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix

def generate_data(n):

 #prva klasa
 
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 =int( n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 #np.random.seed(242)
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data
 
ucenje=(generate_data(200))
test=generate_data(100)
xt=np.column_stack((ucenje[:,0],ucenje[:,1]))
x_test=np.column_stack((test[:,0],test[:,1]))

yt=ucenje[:,2]
ytest=test[:,2]
linearModel = lm.LogisticRegression()
linearModel.fit(xt,yt)    
print ('Model je oblika y_hat = Theta0 + Theta1 * x+ theta2*x2')
print ('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
ytest_p = linearModel.predict(xt)
yte=linearModel.predict(x_test)

a=np.dot(xt,linearModel.coef_.T)+linearModel.intercept_
x2=(-linearModel.intercept_-np.dot(xt[:,0],linearModel.coef_[0,0]))/linearModel.coef_[0,1]
plt.plot(xt[:,0],x2)
plt.scatter(ucenje[:,0],ucenje[:,1],c=ucenje[:,2],cmap='winter')
plt.show()

#vidljivo je kako postoje uzorci izvan područja odnosno ovakva granica odluke dobro dijeli podatke

data_train=ucenje

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
 min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = linearModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logističke regresije')
plt.scatter(ucenje[:,0],ucenje[:,1],c=ucenje[:,2],cmap='pink')
plt.show()

c=yte-ytest
plt.figure(3)
plt.scatter(test[:,0],test[:,1],c=c,cmap='Greens_r')
plt.show()
asd=np.zeros([2,2])
asd=confusion_matrix(ytest,yte)
print(asd)
def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in range(width):
     for y in range(height):
             ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                horizontalalignment='center',
                verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()
 
plot_confusion_matrix(asd)
#izracunavanje dodatnih pokazatelja klasifikacijskog modela
tp=asd[0,0]
fp=asd[0,1]
fn=asd[1,0]
tn=asd[1,1]
acc=(tp+tn)/(tp+tn+fp+fn)
pre=tp/(tp+fp)
print("accuracy",acc)
print("missclacification rate",1-acc)
print("precision",pre)
print("recall",tp/(tp+fn))
print("specificity",tn/(tn+fp))

