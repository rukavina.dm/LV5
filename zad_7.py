# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 20:29:38 2016

@author: laptop
"""

from sklearn.preprocessing import PolynomialFeatures

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix
import math
def generate_data(n):

 #prva klasa
 
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 =int( n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 #np.random.seed(242)
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data
 
ucenje=(generate_data(200))
test=generate_data(100)
data_train=ucenje
poly = PolynomialFeatures(degree=2, include_bias = False)
data_train_new = poly.fit_transform(data_train[:,0:2])

poly = PolynomialFeatures(degree=2, include_bias = False)
data_test_new = poly.fit_transform(test[:,0:2])

x1=np.column_stack((data_train_new [:,0],data_train_new [:,1]))
x2=np.column_stack((data_train_new [:,2],data_train_new [:,3]))
x=np.column_stack((x1,x2))
xt=np.column_stack((x,data_train_new [:,4]))

x_test1=np.column_stack((data_test_new[:,0],data_test_new[:,1]))
x_test2=np.column_stack((data_test_new[:,2],data_test_new[:,3]))
x_test3=np.column_stack((x_test1,x_test2))
x_test=np.column_stack((x_test3,data_test_new[:,4]))


yt=ucenje[:,2]
ytest=test[:,2]
linearModel = lm.LogisticRegression()
linearModel.fit(xt,yt)    
print ('Model je oblika y_hat = Theta0 + Theta1 * x+ theta2*x2')
print ('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
ytest_p = linearModel.predict(xt)
yte=linearModel.predict(x_test)

a=np.dot(xt,linearModel.coef_.T)+linearModel.intercept_
x2=(-linearModel.intercept_-np.dot(xt[:,0],linearModel.coef_[0,0]))/linearModel.coef_[0,1]
plt.plot(xt[:,0],x2)
plt.scatter(ucenje[:,0],ucenje[:,1],c=ucenje[:,2],cmap='winter')
plt.show()

#f, ax = plt.subplots(figsize=(8, 6))
#x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
# min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
#grid = np.c_[x_grid.ravel(), y_grid.ravel()]
#probs = linearModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
#cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
#ax_c = f.colorbar(cont)
#ax_c.set_label("$P(y = 1|\mathbf{x})$")
#ax_c.set_ticks([0, .25, .5, .75, 1])
#ax.set_xlabel('$x_1$', alpha=0.9)
#ax.set_ylabel('$x_2$', alpha=0.9)
#ax.set_title('Izlaz logističke regresije')
#plt.scatter(ucenje[:,0],ucenje[:,1],c=ucenje[:,2],cmap='pink')
#plt.show()

c=yte-ytest
plt.figure(3)
plt.scatter(test[:,0],test[:,1],c=c,cmap='Greens_r')
plt.show()
asd=np.zeros([2,2])
asd=confusion_matrix(ytest,yte)
print(asd)
def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in range(width):
     for y in range(height):
             ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                horizontalalignment='center',
                verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()
 
plot_confusion_matrix(asd)

def plot_KNN(KNN_model, X, y):

 x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
 x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
 xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
 np.arange(x2_min, x2_max, 0.01))

 Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
 Z = Z1.reshape(xx.shape)
 plt.figure()
 plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)

 plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu',
 edgecolor='white', label = 'train')
 
 #izracunavanje dodatnih pokazatelja klasifikacijskog modela
tp=asd[0,0]
fp=asd[0,1]
fn=asd[1,0]
tn=asd[1,1]
acc=(tp+tn)/(tp+tn+fp+fn)
pre=tp/(tp+fp)
print("accuracy",acc)
print("missclacification rate",1-acc)
print("precision",pre)
print("recall",tp/(tp+fn))
print("specificity",tn/(tn+fp))
