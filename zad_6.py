# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 11:24:03 2016

@author: laptop
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix

def generate_data(n):

 #prva klasa
 
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 =int( n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 #np.random.seed(242)
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data
 
ucenje=(generate_data(200))
test=generate_data(100)
xt=np.column_stack((ucenje[:,0],ucenje[:,1]))
x_test=np.column_stack((test[:,0],test[:,1]))

yt=ucenje[:,2]
ytest=test[:,2]
linearModel = lm.LogisticRegression()
linearModel.fit(xt,yt)    
print ('Model je oblika y_hat = Theta0 + Theta1 * x+ theta2*x2')
print ('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
ytest_p = linearModel.predict(xt)
yte=linearModel.predict(x_test)



asd=np.zeros([2,2])
asd=confusion_matrix(ytest,yte)
print(asd)

tp=asd[0,0]
fp=asd[0,1]
fn=asd[1,0]
tn=asd[1,1]
acc=(tp+tn)/(tp+tn+fp+fn)
pre=tp/(tp+fp)
print("accuracy",acc)
print("missclacification rate",1-acc)
print("precision",pre)
print("recall",tp/(tp+fn))
print("specificity",tn/(tn+fp))
