# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 10:12:38 2015

@author: laptop
"""
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

def generate_data(n):

 #prva klasa
 
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 =int( n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 #np.random.seed(242)
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data
 
ucenje=(generate_data(200))#generiranje podataka
test=generate_data(100)    #........
    
#for i in range(0,200):
#    if(ucenje[i,2]==0):
#            
#            plt.scatter(ucenje[i,0],ucenje[i,1],5)
#            
#    else:
#        plt.scatter(ucenje[i,0],ucenje[i,1],25)
#        plt.show() 
plt.scatter(ucenje[:,0],ucenje[:,1],c=ucenje[:,2],cmap='winter')#iscrtavanje podataka u ravnini x1-x2
plt.show()              #pomocu cmop postavljena boja
#http://matplotlib.org/examples/color/colormaps_reference.html